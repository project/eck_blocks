ECK_Blocks 7.x-1.x-dev, xxxx-xx-xx
----------------------------------
#2983708 by DamienMcKenna: Add initial test coverage.


ECK_Blocks 7.x-1.0-beta1, 2018-07-04
------------------------------------
By revagomes: Contributions by DamienMcKenna, willieseabrook: First commit of
  eck_blocks module to drupal.org.
#2983699 by DamienMcKenna: Add a CHANGELOG.txt file.
#2840290 by DamienMcKenna: Don't allow blocks to be created if entity name &
  bundle name combined are over 30 chars.
#2970918 by DamienMcKenna: Allow ECK entities to be displayed as blocks.
